# must
adb shell pm uninstall com.google.mainline.telemetry
adb shell pm uninstall --user 0 com.google.mainline.telemetry
adb shell pm uninstall com.google.android.onetimeinitializer
adb shell pm uninstall --user 0 com.google.android.onetimeinitializer
adb shell pm uninstall com.google.android.setupwizard
adb shell pm uninstall --user 0 com.google.android.setupwizard
adb shell pm uninstall com.google.mainline.adservices
adb shell pm uninstall --user 0 com.google.mainline.adservices

# google assistant
adb shell pm uninstall com.google.android.apps.googleassistant
adb shell pm uninstall --user 0 com.google.android.apps.googleassistant
adb shell pm uninstall com.android.hotwordenrollment.xgoogle
adb shell pm uninstall --user 0 com.android.hotwordenrollment.xgoogle
adb shell pm uninstall com.android.hotwordenrollment.okgoogle
adb shell pm uninstall --user 0 com.android.hotwordenrollment.okgoogle

# google meet
adb shell pm uninstall com.google.android.apps.tachyon
adb shell pm uninstall --user 0 com.google.android.apps.tachyon

# google
adb shell pm uninstall com.google.android.googlequicksearchbox
adb shell pm uninstall --user 0 com.google.android.googlequicksearchbox

# gmail
adb shell pm uninstall com.google.android.gm
adb shell pm uninstall --user 0 com.google.android.gm

# messages
#adb shell pm uninstall com.google.android.apps.messaging
#adb shell pm uninstall --user 0 com.google.android.apps.messaging

# phone
#adb shell pm uninstall com.google.android.dialer
#adb shell pm uninstall --user 0 com.google.android.dialer

# play store
adb shell pm uninstall com.android.vending
adb shell pm uninstall --user 0 com.android.vending

# health connect
adb shell pm uninstall com.google.android.healthconnect.controller
adb shell pm uninstall --user 0 com.google.android.healthconnect.controller

# google news
adb shell pm uninstall com.google.android.apps.magazines
adb shell pm uninstall --user 0 com.google.android.apps.magazines

# youtube music
adb shell pm uninstall com.google.android.apps.youtube.music
adb shell pm uninstall --user 0 com.google.android.apps.youtube.music

# android auto
adb shell pm uninstall com.google.android.projection.gearhead
adb shell pm uninstall --user 0 com.google.android.projection.gearhead

# google tv
adb shell pm uninstall com.google.android.videos
adb shell pm uninstall --user 0 com.google.android.videos

# google pay
adb shell pm uninstall com.google.android.apps.nbu.paisa.user
adb shell pm uninstall --user 0 com.google.android.apps.nbu.paisa.user

# chrome cast
adb shell pm uninstall com.google.android.apps.chromecast.app
adb shell pm uninstall --user 0 com.google.android.apps.chromecast.app

# talk back for disabled, accessibilty feature
adb shell pm uninstall com.google.android.marvin.talkback
adb shell pm uninstall --user 0 com.google.android.marvin.talkback

# safety hub
adb shell pm uninstall com.google.android.apps.safetyhub
adb shell pm uninstall --user 0 com.google.android.apps.safetyhub

# google one
adb shell pm uninstall com.google.android.apps.subscriptions.red
adb shell pm uninstall --user 0 com.google.android.apps.subscriptions.red

# text to speech
adb shell pm uninstall com.google.android.tts
adb shell pm uninstall --user 0 com.google.android.tts

# google podcasts
adb shell pm uninstall com.google.android.apps.podcasts
adb shell pm uninstall --user 0 com.google.android.apps.podcasts

# youtube
adb shell pm uninstall com.google.android.youtube
adb shell pm uninstall --user 0 com.google.android.youtube

# google photos
adb shell pm uninstall com.google.android.apps.photos
adb shell pm uninstall --user 0 com.google.android.apps.photos

# something about smart intelligence services
#adb shell pm uninstall com.google.android.ondevicepersonalization.services
#adb shell pm uninstall --user 0 com.google.android.ondevicepersonalization.services

# private compute services
# com.google.android.federatedcompute

# google files
adb shell pm uninstall com.google.android.apps.nbu.files
adb shell pm uninstall --user 0 com.google.android.apps.nbu.files

# google phone app
#adb shell pm uninstall com.google.android.dialer
#adb shell pm uninstall --user 0 com.google.android.dialer

# dont know what this is for
#adb shell pm uninstall com.google.android.adservices.api
#adb shell pm uninstall --user 0 com.google.android.adservices.api
#adb shell pm uninstall com.google.android.overlay.gmsconfig.personalsafety
#adb shell pm uninstall --user 0 com.google.android.overlay.gmsconfig.personalsafety
#adb shell pm uninstall com.google.android.partnersetup
#adb shell pm uninstall --user 0 com.google.android.partnersetup
adb shell pm uninstall com.google.android.feedback
adb shell pm uninstall --user 0 com.google.android.feedback

# gboard
#adb shell pm uninstall com.google.android.inputmethod.latin
#adb shell pm uninstall --user 0 com.google.android.inputmethod.latin

# chrome
adb shell pm uninstall com.android.chrome
adb shell pm uninstall --user 0 com.android.chrome

# drive
adb shell pm uninstall com.google.android.apps.docs
adb shell pm uninstall --user 0 com.google.android.apps.docs
