# facebook app
adb shell pm uninstall com.facebook.katana
adb shell pm uninstall --user 0 com.facebook.katana

# background services
adb shell pm uninstall com.facebook.appmanager
adb shell pm uninstall --user 0 com.facebook.appmanager
adb shell pm uninstall com.facebook.services
adb shell pm uninstall --user 0 com.facebook.services
adb shell pm uninstall com.facebook.system
adb shell pm uninstall --user 0 com.facebook.system
