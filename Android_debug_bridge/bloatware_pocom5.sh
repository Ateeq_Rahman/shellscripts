# For POCO M5
# must delete

adb shell pm uninstall com.miui.daemon
adb shell pm uninstall --user 0 com.miui.daemon

adb shell pm uninstall com.miui.miservice
adb shell pm uninstall --user 0 com.miui.miservice

adb shell pm uninstall com.mi.android.globalFileexplorer
adb shell pm uninstall --user 0 com.mi.android.globalFileexplorer

adb shell pm uninstall com.miui.msa.global
adb shell pm uninstall --user 0 com.miui.msa.global

#adb shell pm uninstall com.linkedin.android
#adb shell pm uninstall --user 0 com.linkedin.android

adb shell pm uninstall com.mi.android.globalminusscreen
adb shell pm uninstall --user 0 com.mi.android.globalminusscreen

#adb shell pm uninstall com.google.android.apps.nbu.files
#adb shell pm uninstall --user 0 com.google.android.apps.nbu.files

adb shell pm uninstall com.miui.fm
adb shell pm uninstall --user 0 com.miui.fm

#adb shell pm uninstall com.huaqin.sarcontroller
#adb shell pm uninstall --user 0 com.huaqin.sarcontroller

adb shell pm uninstall com.facebook.appmanager
adb shell pm uninstall --user 0 com.facebook.appmanager

adb shell pm uninstall com.xiaomi.xmsf
adb shell pm uninstall --user 0 com.xiaomi.xmsf

adb shell pm uninstall in.amazon.mShop.android.shopping
adb shell pm uninstall --user 0 in.amazon.mShop.android.shopping

adb shell pm uninstall com.xiaomi.micloud.sdk
adb shell pm uninstall --user 0 com.xiaomi.micloud.sdk

adb shell pm uninstall com.miui.phone.carriers.overlay.vodafone
adb shell pm uninstall --user 0 com.miui.phone.carriers.overlay.vodafone

#adb shell pm uninstall com.miui.global.packageinstaller
#adb shell pm uninstall --user 0 com.miui.global.packageinstaller

adb shell pm uninstall com.miui.micloudsync
adb shell pm uninstall --user 0 com.miui.micloudsync

adb shell pm uninstall com.google.android.adservices.api
adb shell pm uninstall --user 0 com.google.android.adservices.api

#not uninstallable through adb, requires manual intervention 
adb shell pm uninstall com.xiaomi.scanner
adb shell pm uninstall --user 0 com.xiaomi.scanner

adb shell pm uninstall com.google.android.ondevicepersonalization.services
adb shell pm uninstall --user 0 com.google.android.ondevicepersonalization.services

#not uninstallable through adb, requires manual intervention 
adb shell pm uninstall com.miui.weather2
adb shell pm uninstall --user 0 com.miui.weather2

adb shell pm uninstall com.miui.analytics
adb shell pm uninstall --user 0 com.miui.analytics

adb shell pm uninstall com.xiaomi.joyose
adb shell pm uninstall --user 0 com.xiaomi.joyose

#adb shell pm uninstall com.miui.audiomonitor
#adb shell pm uninstall --user 0 com.miui.audiomonitor

adb shell pm uninstall com.xiaomi.account
adb shell pm uninstall --user 0 com.xiaomi.account

adb shell pm uninstall com.amazon.avod.thirdpartyclient
adb shell pm uninstall --user 0 com.amazon.avod.thirdpartyclient

adb shell pm uninstall com.google.android.apps.googleassistant
adb shell pm uninstall --user 0 com.google.android.apps.googleassistant

#adb shell pm uninstall com.huaqin.diaglogger
#adb shell pm uninstall --user 0 com.huaqin.diaglogger

adb shell pm uninstall com.xiaomi.payment
adb shell pm uninstall --user 0 com.xiaomi.payment

adb shell pm uninstall com.google.android.apps.tachyon
adb shell pm uninstall --user 0 com.google.android.apps.tachyon

adb shell pm uninstall com.google.android.apps.magazines
adb shell pm uninstall --user 0 com.google.android.apps.magazines

adb shell pm uninstall com.xiaomi.simactivate.service
adb shell pm uninstall --user 0 com.xiaomi.simactivate.service

adb shell pm uninstall com.spotify.music
adb shell pm uninstall --user 0 com.spotify.music

#adb shell pm uninstall com.huaqin.clearefs
#adb shell pm uninstall --user 0 com.huaqin.clearefs

adb shell pm uninstall com.miui.cloudbackup
adb shell pm uninstall --user 0 com.miui.cloudbackup

adb shell pm uninstall com.google.android.dialer
adb shell pm uninstall --user 0 com.google.android.dialer

adb shell pm uninstall com.xiaomi.glgm
adb shell pm uninstall --user 0 com.xiaomi.glgm

#not uninstallable through adb, requires manual intervention 
adb shell pm uninstall com.android.soundrecorder
adb shell pm uninstall --user 0 com.android.soundrecorder

adb shell pm uninstall com.netflix.mediaclient
adb shell pm uninstall --user 0 com.netflix.mediaclient

adb shell pm uninstall com.google.mainline.adservices
adb shell pm uninstall --user 0 com.google.mainline.adservices

#not uninstallable through adb, requires manual intervention 
adb shell pm uninstall com.xiaomi.midrop
adb shell pm uninstall --user 0 com.xiaomi.midrop

adb shell pm uninstall com.android.hotwordenrollment.okgoogle
adb shell pm uninstall --user 0 com.android.hotwordenrollment.okgoogle

adb shell pm uninstall com.miui.player
adb shell pm uninstall --user 0 com.miui.player

adb shell pm uninstall com.google.android.overlay.gmsconfig.personalsafety
adb shell pm uninstall --user 0 com.google.android.overlay.gmsconfig.personalsafety

adb shell pm uninstall com.duokan.phone.remotecontroller
adb shell pm uninstall --user 0 com.duokan.phone.remotecontroller

#not uninstallable through adb, requires manual intervention 
adb shell pm uninstall com.miui.compass
adb shell pm uninstall --user 0 com.miui.compass

adb shell pm uninstall com.mintgames.zentriple3d
adb shell pm uninstall --user 0 com.mintgames.zentriple3d

adb shell pm uninstall com.android.hotwordenrollment.xgoogle
adb shell pm uninstall --user 0 com.android.hotwordenrollment.xgoogle

adb shell pm uninstall com.snapchat.android
adb shell pm uninstall --user 0 com.snapchat.android

adb shell pm uninstall com.google.android.apps.youtube.music
adb shell pm uninstall --user 0 com.google.android.apps.youtube.music

adb shell pm uninstall com.google.android.partnersetup
adb shell pm uninstall --user 0 com.google.android.partnersetup

adb shell pm uninstall com.google.android.projection.gearhead
adb shell pm uninstall --user 0 com.google.android.projection.gearhead

#not uninstallable through adb, requires manual intervention 
adb shell pm uninstall com.miui.notes
adb shell pm uninstall --user 0 com.miui.notes

adb shell pm uninstall com.google.android.videos
adb shell pm uninstall --user 0 com.google.android.videos

adb shell pm uninstall com.xiaomi.finddevice
adb shell pm uninstall --user 0 com.xiaomi.finddevice

adb shell pm uninstall com.google.android.feedback
adb shell pm uninstall --user 0 com.google.android.feedback

adb shell pm uninstall com.xiaomi.xmsfkeeper
adb shell pm uninstall --user 0 com.xiaomi.xmsfkeeper

adb shell pm uninstall com.android.chrome
adb shell pm uninstall --user 0 com.android.chrome

adb shell pm uninstall android.autoinstalls.config.Xiaomi.model
adb shell pm uninstall --user 0 android.autoinstalls.config.Xiaomi.model

adb shell pm uninstall com.facebook.services
adb shell pm uninstall --user 0 com.facebook.services

adb shell pm uninstall com.google.android.apps.nbu.paisa.user
adb shell pm uninstall --user 0 com.google.android.apps.nbu.paisa.user

adb shell pm uninstall com.preff.kb.xm
adb shell pm uninstall --user 0 com.preff.kb.xm

adb shell pm uninstall com.google.android.apps.chromecast.app
adb shell pm uninstall --user 0 com.google.android.apps.chromecast.app

adb shell pm uninstall com.mintgames.schuman.ablock
adb shell pm uninstall --user 0 com.mintgames.schuman.ablock

#adb shell pm uninstall com.google.android.inputmethod.latin
#adb shell pm uninstall --user 0 com.google.android.inputmethod.latin

adb shell pm uninstall com.miui.bugreport
adb shell pm uninstall --user 0 com.miui.bugreport

adb shell pm uninstall com.google.android.marvin.talkback
adb shell pm uninstall --user 0 com.google.android.marvin.talkback

adb shell pm uninstall com.miui.yellowpage
adb shell pm uninstall --user 0 com.miui.yellowpage

adb shell pm uninstall cn.wps.xiaomi.abroad.lite
adb shell pm uninstall --user 0 cn.wps.xiaomi.abroad.lite

adb shell pm uninstall com.opera.browser.afin
adb shell pm uninstall --user 0 com.opera.browser.afin

adb shell pm uninstall com.miui.fmservice
adb shell pm uninstall --user 0 com.miui.fmservice

adb shell pm uninstall com.google.android.setupwizard
adb shell pm uninstall --user 0 com.google.android.setupwizard

adb shell pm uninstall com.google.android.apps.safetyhub
adb shell pm uninstall --user 0 com.google.android.apps.safetyhub

adb shell pm uninstall com.xiaomi.calendar
adb shell pm uninstall --user 0 com.xiaomi.calendar

adb shell pm uninstall com.miui.backup
adb shell pm uninstall --user 0 com.miui.backup

adb shell pm uninstall com.myntra.android
adb shell pm uninstall --user 0 com.myntra.android

#notification tools
#adb shell pm uninstall com.xiaomi.barrage
#adb shell pm uninstall --user 0 com.xiaomi.barrage

adb shell pm uninstall com.google.android.apps.subscriptions.red
adb shell pm uninstall --user 0 com.google.android.apps.subscriptions.red

adb shell pm uninstall com.google.android.apps.messaging
adb shell pm uninstall --user 0 com.google.android.apps.messaging

adb shell pm uninstall com.google.android.tts
adb shell pm uninstall --user 0 com.google.android.tts

adb shell pm uninstall com.google.android.apps.podcasts
adb shell pm uninstall --user 0 com.google.android.apps.podcasts

adb shell pm uninstall com.android.mms
adb shell pm uninstall --user 0 com.android.mms

adb shell pm uninstall com.miui.miinput
adb shell pm uninstall --user 0 com.miui.miinput

adb shell pm uninstall com.miui.cloudservice
adb shell pm uninstall --user 0 com.miui.cloudservice

adb shell pm uninstall com.milink.service
adb shell pm uninstall --user 0 com.milink.service

#adb shell pm uninstall com.miui.misound
#adb shell pm uninstall --user 0 com.miui.misound

adb shell pm uninstall com.funnypuri.client
adb shell pm uninstall --user 0 com.funnypuri.client

adb shell pm uninstall com.facebook.system
adb shell pm uninstall --user 0 com.facebook.system

adb shell pm uninstall com.google.mainline.telemetry
adb shell pm uninstall --user 0 com.google.mainline.telemetry

adb shell pm uninstall com.facebook.katana
adb shell pm uninstall --user 0 com.facebook.katana

adb shell pm uninstall org.mipay.android.manager
adb shell pm uninstall --user 0 org.mipay.android.manager

adb shell pm uninstall com.tencent.soter.soterserver
adb shell pm uninstall --user 0 com.tencent.soter.soterserver

adb shell pm uninstall com.xiaomi.mi_care
adb shell pm uninstall --user 0 com.xiaomi.mi_care

adb shell pm uninstall com.google.android.youtube
adb shell pm uninstall --user 0 com.google.android.youtube

adb shell pm uninstall com.miui.mediaeditor
adb shell pm uninstall --user 0 com.miui.mediaeditor

adb shell pm uninstall com.xiaomi.smarthome
adb shell pm uninstall --user 0 com.xiaomi.smarthome

adb shell pm uninstall com.block.juggle
adb shell pm uninstall --user 0 com.block.juggle

adb shell pm uninstall com.mi.globalminusscreen
adb shell pm uninstall --user 0 com.mi.globalminusscreen

