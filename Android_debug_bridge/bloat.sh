# linkedin
adb shell pm uninstall com.linkedin.android
adb shell pm uninstall --user 0 com.linkedin.android

# spotify
adb shell pm uninstall com.spotify.music
adb shell pm uninstall --user 0 com.spotify.music

# snapchat
adb shell pm uninstall com.snapchat.android
adb shell pm uninstall --user 0 com.snapchat.android

# opera browser
adb shell pm uninstall com.opera.browser.afin
adb shell pm uninstall --user 0 com.opera.browser.afin

# myntra
adb shell pm uninstall com.myntra.android
adb shell pm uninstall --user 0 com.myntra.android

# zili
adb shell pm uninstall com.funnypuri.client
adb shell pm uninstall --user 0 com.funnypuri.client

# netflix
adb shell pm uninstall com.netflix.mediaclient
adb shell pm uninstall --user 0 com.netflix.mediaclient

# games
adb shell pm uninstall com.mintgames.zentriple3d
adb shell pm uninstall --user 0 com.mintgames.zentriple3d
adb shell pm uninstall com.block.juggle
adb shell pm uninstall --user 0 com.block.juggle
adb shell pm uninstall com.mintgames.zentriple3d
adb shell pm uninstall --user 0 com.mintgames.zentriple3d
adb shell pm uninstall com.mintgames.schuman.ablock
adb shell pm uninstall --user 0 com.mintgames.schuman.ablock
