# must
adb shell pm uninstall android.autoinstalls.config.Xiaomi.model
adb shell pm uninstall --user 0 android.autoinstalls.config.Xiaomi.model
adb shell pm uninstall com.tencent.soter.soterserver
adb shell pm uninstall --user 0 com.tencent.soter.soterserver
adb shell pm uninstall com.miui.daemon
adb shell pm uninstall --user 0 com.miui.daemon
adb shell pm uninstall com.miui.msa.global
adb shell pm uninstall --user 0 com.miui.msa.global
adb shell pm uninstall com.miui.analytics
adb shell pm uninstall --user 0 com.miui.analytics

# fm 
adb shell pm uninstall com.miui.fm
adb shell pm uninstall --user 0 com.miui.fm
adb shell pm uninstall com.miui.fmservice
adb shell pm uninstall --user 0 com.miui.fmservice

# vodafone
adb shell pm uninstall com.miui.phone.carriers.overlay.vodafone
adb shell pm uninstall --user 0 com.miui.phone.carriers.overlay.vodafone

# sim activation service
adb shell pm uninstall com.xiaomi.simactivate.service
adb shell pm uninstall --user 0 com.xiaomi.simactivate.service

# messaging
adb shell pm uninstall com.android.mms
adb shell pm uninstall --user 0 com.android.mms

# for some reason this service is constantly uploading and downloading packages.
adb shell pm uninstall com.xiaomi.bluetooth
adb shell pm uninstall --user 0 com.xiaomi.bluetooth

# this is for face biometric
#adb shell pm uninstall com.miui.face
#adb shell pm uninstall --user 0 com.miui.face

# services and feedback app
adb shell pm uninstall com.miui.miservice
adb shell pm uninstall --user 0 com.miui.miservice

# package installer
adb shell pm uninstall com.miui.global.packageinstaller
adb shell pm uninstall --user 0 com.miui.global.packageinstaller

# wallpaper carousel
adb shell pm uninstall com.miui.android.fashiongallery
adb shell pm uninstall --user 0 com.miui.android.fashiongallery

# mi cloud
adb shell pm uninstall com.xiaomi.account
adb shell pm uninstall --user 0 com.xiaomi.account
adb shell pm uninstall com.miui.micloudsync
adb shell pm uninstall --user 0 com.miui.micloudsync
adb shell pm uninstall com.miui.cloudbackup
adb shell pm uninstall --user 0 com.miui.cloudbackup
adb shell pm uninstall com.miui.backup
adb shell pm uninstall --user 0 com.miui.backup
adb shell pm uninstall com.miui.cloudservice
adb shell pm uninstall --user 0 com.miui.cloudservice
adb shell pm uninstall com.xiaomi.micloud.sdk
adb shell pm uninstall --user 0 com.xiaomi.micloud.sdk

# weather app
adb shell pm uninstall com.miui.weather2
adb shell pm uninstall --user 0 com.miui.weather2

# music
adb shell pm uninstall com.miui.player
adb shell pm uninstall --user 0 com.miui.player

# compass
adb shell pm uninstall com.miui.compass
adb shell pm uninstall --user 0 com.miui.compass

# notes
adb shell pm uninstall com.miui.notes
adb shell pm uninstall --user 0 com.miui.notes

# feedback
adb shell pm uninstall com.miui.bugreport
adb shell pm uninstall --user 0 com.miui.bugreport

# yellow pages
adb shell pm uninstall com.miui.yellowpage
adb shell pm uninstall --user 0 com.miui.yellowpage

# mi sound
#adb shell pm uninstall com.miui.misound
#adb shell pm uninstall --user 0 com.miui.misound

# gallery
adb shell pm uninstall com.miui.gallery
adb shell pm uninstall --user 0 com.miui.gallery
adb shell pm uninstall com.gallery.player
adb shell pm uninstall --user 0 com.gallery.player
adb shell pm uninstall com.miui.mediaeditor
adb shell pm uninstall --user 0 com.miui.mediaeditor

# file manager
adb shell pm uninstall com.mi.android.globalFileexplorer
adb shell pm uninstall --user 0 com.mi.android.globalFileexplorer

# mi framework
adb shell pm uninstall com.xiaomi.xmsf
adb shell pm uninstall --user 0 com.xiaomi.xmsf
adb shell pm uninstall com.xiaomi.xmsfkeeper
adb shell pm uninstall --user 0 com.xiaomi.xmsfkeeper

# scanner
adb shell pm uninstall com.xiaomi.scanner
adb shell pm uninstall --user 0 com.xiaomi.scanner

# so called "getapps", mostly useless
adb shell pm uninstall com.xiaomi.mipicks
adb shell pm uninstall --user 0 com.xiaomi.mipicks

# joyose
adb shell pm uninstall com.xiaomi.joyose
adb shell pm uninstall --user 0 com.xiaomi.joyose

# theme
#adb shell pm uninstall com.android.thememanager
#adb shell pm uninstall --user 0 com.android.thememanager

# payment
adb shell pm uninstall com.xiaomi.payment
adb shell pm uninstall --user 0 com.xiaomi.payment
adb shell pm uninstall com.mipay.android.manager
adb shell pm uninstall --user 0 com.mipay.android.manager
adb shell pm uninstall org.mipay.android.manager
adb shell pm uninstall --user 0 org.mipay.android.manager

# game recommendation
adb shell pm uninstall com.xiaomi.glgm
adb shell pm uninstall --user 0 com.xiaomi.glgm

# share me
adb shell pm uninstall com.xiaomi.midrop
adb shell pm uninstall --user 0 com.xiaomi.midrop

# find device
adb shell pm uninstall com.xiaomi.finddevice
adb shell pm uninstall --user 0 com.xiaomi.finddevice

# document viewer
adb shell pm uninstall cn.wps.xiaomi.abroad.lite
adb shell pm uninstall --user 0 cn.wps.xiaomi.abroad.lite

# calendar
adb shell pm uninstall com.xiaomi.calendar
adb shell pm uninstall --user 0 com.xiaomi.calendar

# mi care
adb shell pm uninstall com.xiaomi.mi_care
adb shell pm uninstall --user 0 com.xiaomi.mi_care

# mi home
adb shell pm uninstall com.xiaomi.smarthome
adb shell pm uninstall --user 0 com.xiaomi.smarthome

# always on display
# com.miui.aod

# app vault
adb shell pm uninstall com.mi.android.globalminusscreen
adb shell pm uninstall --user 0 com.mi.android.globalminusscreen
adb shell pm uninstall com.mi.globalminusscreen
adb shell pm uninstall --user 0 com.mi.globalminusscreen

# mi ai engine
adb shell pm uninstall com.xiaomi.aicr
adb shell pm uninstall --user 0 com.xiaomi.aicr

# dont know
adb shell pm uninstall com.miui.phrase
adb shell pm uninstall --user 0 com.miui.phrase

# mi remote
adb shell pm uninstall com.duokan.phone.remotecontroller
adb shell pm uninstall --user 0 com.duokan.phone.remotecontroller

# system app updater
adb shell pm uninstall com.xiaomi.discover
adb shell pm uninstall --user 0 com.xiaomi.discover

# cast
adb shell pm uninstall com.milink.service
adb shell pm uninstall --user 0 com.milink.service

# I dont know
#adb shell pm uninstall com.miui.audiomonitor
#adb shell pm uninstall --user 0 com.miui.audiomonitor
#adb shell pm uninstall com.miui.miinput
#adb shell pm uninstall --user 0 com.miui.miinput
#adb shell pm uninstall com.huaqin.sarcontroller
#adb shell pm uninstall --user 0 com.huaqin.sarcontroller
#adb shell pm uninstall com.huaqin.diaglogger
#adb shell pm uninstall --user 0 com.huaqin.diaglogger
#adb shell pm uninstall com.huaqin.clearefs
#adb shell pm uninstall --user 0 com.huaqin.clearefs
# com.xiaomi.trustservice
#adb shell pm uninstall com.miui.audiomonitor
#adb shell pm uninstall --user 0 com.miui.audiomonitor
# clock
adb shell pm uninstall com.android.deskclock
adb shell pm uninstall --user 0 com.android.deskclock
