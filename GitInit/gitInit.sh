# create a local repo
cd ~
mkdir -p <localRepo>
cd <localRepo>
git init
git config --global init.defaultBranch main
git branch -m main
git config --global user.name "<username>"
git config --global user.email "<mail>"
git pull <repo>
git remote add origin <repo>

# push changes to remote repo
git add .
git commit -m "<message>"
git push --set-upstream origin main
#git push
